# NB: This is a sketch of an idea. 

We are not sure it will ever be as good as we describe here. :-)

# Templistic

A minimal HTML component web library. 

It uses the standard HTML `<template...>` tags, but without the need for writing any javascript.

# Getting started / TL;DR 

Add to your head-tag:
```
<html>
<head>
    ...
  <script src="templistic-0.0.1.js"></script>
    ...
</head>
...
```
This will load the file `templates.html` (a sensible default) with your templates from the same directory. 

Add templates in `templates.html` (see https://web.dev/learn/html/template/ for syntax details), e.g:

```
<template id="my-paragraph">
  <style>
    p {
      color: white;
      background-color: #666;
      padding: 5px;
    }
  </style>
  <p><slot name="my-text">My default text</slot></p>
</template>
```

You can add your own templates, or any pre-cooked templates from our vast component library (coming soon) by adding the following _before_ the templistic-x.y.z.js script tag:

```
<html>
<head>
  ...
  <script>var templistic_templates = ["your_templates.html", ...];</script>
  <script src="templistic-0.0.1.js"></script>
  ...
</head>
...
```

# Philosophy

Templistic should be an abstraction layer on top of/next to your favorite framework.

Instead creating a hero section like this, in Tailwind:
```html
<section class="bg-white dark:bg-gray-900">
  <div class="grid max-w-screen-xl px-4 py-8 mx-auto lg:gap-8 xl:gap-0 lg:py-16 lg:grid-cols-12">
    <div class="mr-auto place-self-center lg:col-span-7">
      <h1 class="max-w-2xl mb-4 text-4xl font-extrabold tracking-tight leading-none md:text-5xl xl:text-6xl dark:text-white">Payments tool for software companies</h1>
      <p class="max-w-2xl mb-6 font-light text-gray-500 lg:mb-8 md:text-lg lg:text-xl dark:text-gray-400">From checkout to global sales tax compliance, companies around the world use Flowbite to simplify their payment stack.</p>
      <a href="#" class="inline-flex items-center justify-center px-5 py-3 mr-3 text-base font-medium text-center text-white rounded-lg bg-primary-700 hover:bg-primary-800 focus:ring-4 focus:ring-primary-300 dark:focus:ring-primary-900">
        Get started
        <svg class="w-5 h-5 ml-2 -mr-1" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M10.293 3.293a1 1 0 011.414 0l6 6a1 1 0 010 1.414l-6 6a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-4.293-4.293a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
      </a>
      <a href="#" class="inline-flex items-center justify-center px-5 py-3 text-base font-medium text-center text-gray-900 border border-gray-300 rounded-lg hover:bg-gray-100 focus:ring-4 focus:ring-gray-100 dark:text-white dark:border-gray-700 dark:hover:bg-gray-700 dark:focus:ring-gray-800">
        Speak to Sales
      </a> 
    </div>
    <div class="hidden lg:mt-0 lg:col-span-5 lg:flex">
      <img src="https://flowbite.s3.amazonaws.com/blocks/marketing-ui/hero/phone-mockup.png" alt="mockup">
    </div>
  </div>
</section>
```
...we aim to offer functionality like this (`ttic` is short for `templistic`):

```html
<ttic-hero-with-image>
  <span slot="title">Payments tool for software companies</span>
  <span slot="body">From checkout to global sales tax compliance, companies around the world use Flowbite to simplify their payment stack.</span>
  <img slot="image" src="https://flowbite.s3.amazonaws.com/blocks/marketing-ui/hero/phone-mockup.png" alt="mockup">
  <ttic-primary-button slot="primary-action" href="#">
    <span slot="title">Get started</span>
  </ttic-primary-button>
  <ttic-secondary-button slot="secondary-action" href="#">
    <span slot="title">Speak to sales</span>
  </ttic-secondary-button>
</ttic-hero-with-image>
```

Our components will be opinionated, but do not fret. You can always use the underlying framework to create whatever you want. If you create something that is missing, we do appreciate pull requests.

# Create a nice, responsive, website

## Start with a standard container

```html
<ttic-page>
  <ttic-top-menu slot="top-menu">
    ...
  </ttic-top-menu>
  <ttic-horizontal-menu slot="left-menu">
    ...
  </ttic-horizontal-menu>
  <ttic-body slot="body">
    ...
  </ttic-body>
  <ttic-footer slot="footer">
    ...
  </ttic-footer>
</ttic-page>
```

## Add top menu items

```html
<ttic-top-menu slot="top-menu">
  <ttic-logo-icon-name slot="menu-logo" href="/home.html" img="assets/logo.svg" img_alt="Logo image" name="Hem"></ttic-logo-icon-name>
  <ttic-menu-item slot="menu-item" href="/about.html" title="Om oss"></ttic-menu-item>
  <ttic-menu-item slot="menu-item" href="/contact.html" title="Kontakt"></ttic-menu-item>
</ttic-top-menu>
```

## Add a footer

```html
<ttic-footer slot="footer>
  <span slot="copyright">© 2023 <a href="https://agical.se/" class="hover:underline">Agical</a>. All Rights Reserved.</span>
  <ttic-footer-link slot="link" href="/about.html">
    <span slot="title">About</span>
  </ttic-footer-link>
  <ttic-footer-link slot="link" href="/privacy.html">
    <span slot="title">Privacy policy</span>
  </ttic-footer-link>
  <ttic-footer-link slot="link" href="/contact.html">
    <span slot="title">Contact</span>
  </ttic-footer-link>
</ttic-footer>
```

Build icon templates from flowbite-icons
```shell
find . -type f | grep svg | \
sed 's#\.\/\([a-z]*\)\/\([^\/]*\)\/\([^\.]*\)\.svg#echo \"<template id=\\\"ttic-\1-\3\\\">\"\ncat \"./\1/\2/\3.svg\"\necho "</template>"#g' | \
sh | \
sed 's/#2F2F39/currentColor/g' | \
sed 's/#2F2F38/currentColor/g' | \
sed 's#<svg xmlns#<svg class="w-6 h-6 text-gray-800 dark:text-white" xmlns#g' > ../../templistic/flowbite-icons.html
```

