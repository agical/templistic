'use strict';

(function() {
  const defaultSlotName = "$$default-slot$$";
  const defaultSlotQuery = "slot:not([name])";
  let debugLogging = false;

  function log() {
    if (debugLogging) {
      console.log(...arguments);
    }
  }

  function createElementFromHTML(html) {
    let div = document.createElement("div");
    div.innerHTML = html;
    return div;
  }

  function prependBody(html) {
    new MutationObserver((records, self) => {
      for (let i = 0; i < records.length; ++i) {
        for (let j = 0; j < records[i].addedNodes.length; ++j) {
          if (records[i].addedNodes[j].nodeName == 'BODY') {
            self.disconnect();
            document.body.insertAdjacentHTML("afterbegin", html);
          }
        }
      }
    }).observe(document.documentElement, { childList: true });
  }

  class TemplisticParameters {
    constructor(content = "") {
      log("TemplisticParameters.constructor()", content);
      this.content = content;
      this.REGEX_UNUSED_ATTRIBUTES = /ttic:[\w-]+/gi;
    }

    update(parameters = []) {
      log("TemplisticParameters.update()", parameters);
      let content = this.content;
      for (const parameter of parameters) {
        if (parameter.name != "slot") {
          content = this.updateParameter(content, parameter);
        }
      }
      content = this.removeUnusedParameters(content);
      log("TemplisticParameters.update(): Updated template parameters:", content);
      return content;
    }

    updateParameter(content, parameter) {
      const re = new RegExp(`ttic:${parameter.name}`, "g");
      return content.replaceAll(re, parameter.value);
    }

    removeUnusedParameters(content) {
      log("TemplisticParameters.removeUnusedParameters()");
      return content.replaceAll(this.REGEX_UNUSED_ATTRIBUTES, "");
    }
  }

  class TemplisticTemplate extends HTMLElement {
    constructor(template) {
      super();
      log("TemplisticTemplate.constructor()", template);
      this.template = template;

      if (this.template.hasAttribute("shadow")) {
        this.setupShadowRoot(this.template);
        this.connected = true;
      } else {
        this.connected = false;
      }
    }

    connectedCallback() {
      if (!this.connected) {
        this.setupManualRoot(this.template);
        this.connected = true;
      }
    }

    setupShadowRoot(template) {
      log("TemplisticTemplate.setupShadowRoot(): Attaching shadow root for template", this.template.id, this.attributes, this.template.content);
      let shadow = this.attachShadow({ mode: template.getAttribute("shadow") });
      shadow.innerHTML = new TemplisticParameters(template.innerHTML).update(this.attributes);
    }

    setupManualRoot(template) {
      log("TemplisticTemplate.setupManualRoot(): Upgrading template", template.id, template.attributes);
      const slottedTemplate = this.updateSlots(template.cloneNode(true), this.buildNodeMap(this.children));
      this.outerHTML = new TemplisticParameters(slottedTemplate.innerHTML).update(this.attributes);
    }

    updateSlots(template, nodeMap) {
      log("TemplisticTemplate.updateSlots()", template, nodeMap);
      for (const [slotName, nodes] of Object.entries(nodeMap)) {
        const slotQuery = (slotName === defaultSlotName) ? defaultSlotQuery : `slot[name='${slotName}']`;
        let templateSlots = template.content.querySelectorAll(slotQuery);
        if (templateSlots.length > 0) {
          templateSlots[0].replaceWith(...nodes);
        }
      }
      log("TemplisticTemplate.updateSlots(): Updated template slots with nodes:", template);
      return template;
    }

    buildNodeMap(nodes) {
      log("TemplisticTemplate.buildNodeMap()", nodes);
      let nodeMap = {};
      for (const node of nodes) {
        log("TemplisticTemplate.buildNodeMap(): node", node);
        const slotName = node.getAttribute("slot") || defaultSlotName;
        node.removeAttribute("slot");
        log("TemplisticTemplate.buildNodeMap(): slotName = ", slotName);
        nodeMap[slotName] ||= [];
        nodeMap[slotName].push(node);
      }
      log("TemplisticTemplate.buildNodeMap(): nodeMap", nodeMap);
      return nodeMap;
    }
  }

  class Templistic {
    constructor(options = {}) {
      log("Templistic.constructor()", options);
      this.options = Object.assign({
        debug: false,
        templates: [],
        loadingOverlay: false,
        loadingOverlayId: "templistic-loading-overlay",
        afterInit: () => {}
      }, options);
      this.options.loadingOverlayHTML ||= `
        <div id="${this.options.loadingOverlayId}" class="w-full h-full fixed block top-0 left-0 bg-white dark:bg-gray-900 z-50">
          <span class="text-green-500 opacity-75 top-1/2 my-0 mx-auto block relative w-0 h-0">
            <div role="status">
              <svg aria-hidden="true" class="inline w-48 h-48 -ml-24 -mt-24 text-gray-200 animate-spin dark:text-gray-600 fill-blue-600" viewBox="0 0 100 101" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z" fill="currentColor"/>
                <path d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z" fill="currentFill"/>
              </svg>
              <span class="sr-only">Loading...</span>
            </div>
          </span>
        </div>
      `;
      debugLogging = options.debug;
    }

    init() {
      if (this.options.loadingOverlay) {
        this.setupLoadingOverlay();
      }
      document.addEventListener("DOMContentLoaded", this.loadTemplates.bind(this));
      return this;
    }

    setupLoadingOverlay() {
      prependBody(this.options.loadingOverlayHTML);
    }

    afterDefineTemplates(futures) {
      log("Templistic.afterDefineTemplates(): Running afterInit script...");
      this.options.afterInit();
      document.getElementById(this.options.loadingOverlayId)?.remove();
      log("Templistic.afterDefineTemplates(): Done afterInit script.");
    }

    loadTemplates() {
      log("Templistic.loadTemplates()");
      Promise.all(this.options.templates.map(async (template) => {
        return await fetch(template)
          .then((response) => response.text())
          .then(createElementFromHTML)
          .then(this.defineTemplateTags.bind(this))
          .catch((error) => {
            console.error(error);
          });
      })).then(this.afterDefineTemplates.bind(this));
    }

    defineTemplateTags(doc) {
      const templates = doc.getElementsByTagName("template");
      for (const template of templates) {
        this.defineTemplateTag(template);
      }
    }

    defineTemplateTag(template) {
      customElements.define(
        template.id,
        class extends TemplisticTemplate {
          constructor() {
            super(template);
          }
        }
      );
    }
  }

  new Templistic(window.templistic_options).init();
})();
